@extends('layout')
@section('title', 'Add New Customer')

@section('content')
    <div class="row">
        <div class="col-12">
            <h2>Add New Customer</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form action="/customers" method="post" enctype="multipart/form-data">
                @csrf

               @include('customers.form')
                <button class="btn btn-primary">Add Data</button>
            </form>
        </div>
    </div>
@endsection
