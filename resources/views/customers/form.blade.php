<div class="form-group">
    <label for="name">Name:</label>
    <input type="text" name="name" value="{{ old('name') ?? $customer->name }}" class="form-control">
    {{ $errors->first('name') }}
</div>

<div class="form-group">
    <label for="email">Email:</label>
    <input type="text" name="email" value="{{ old('email') ?? $customer->email }}" class="form-control">
    {{ $errors->first('email') }}
</div>

<div class="form-group">
    <label for="active">Satus:</label>
    <select name="active" id="active" class="form-control">
        <option value="" disabled>Select Customer Status</option>
        @foreach($customer->activeOptions() as $activeOptionKey => $activeOptionValue)
            <option value="{{ $activeOptionKey }}" {{ $customer->active == $activeOptionValue? 'selected' : '' }}>{{ $activeOptionValue }}</option>
        @endforeach
        {{--<option value="1" {{ $customer->active == 'Active'? 'selected' : '' }}>Active</option>
        <option value="0" {{ $customer->active == 'Inactive'? 'selected' : '' }}>Inactive</option>--}}
    </select>
</div>
<div class="form-group">
    <label for="active">Company:</label>
    <select name="company_id" id="active" class="form-control">
        <option value="" disabled>Select Company</option>
        @foreach($companies as $comapny)
            <option value="{{ $comapny->id }}" {{ $comapny->id ==  $customer->company_id ? 'selected' : ''}}>{{ $comapny->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group d-flex flex-column">
    <label for="image">Profile Image:</label>
    <input type="file" name="image" class="py-1">
    <div>{{ $errors->first('image') }}</div>
</div>


