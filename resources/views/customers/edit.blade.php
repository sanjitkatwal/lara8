@extends('layout')
@section('title', 'Edit Details for '. $customer->name)

@section('content')
    <div class="row">
        <div class="col-12">
            <h2>Edit Details for {{ $customer->name }}</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form action="/customers/{{ $customer->id }}" method="post" enctype="multipart/form-data">
                @method('PATCH')
                @csrf

               @include('customers.form')

                <button class="btn btn-primary">Update Data</button>
            </form>
        </div>
    </div>

@endsection
