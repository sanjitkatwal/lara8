@extends('layout')
@section('title', 'Contact Us')

@section('content')

    <h1>Contact Us</h1>

    @if(! session()->has('message'))
        <form action="/contact" method="post">
            @csrf
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" name="name" value="{{ old('name')}}" class="form-control">
                {{ $errors->first('name') }}
            </div>

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" name="email" value="{{ old('email') }}" class="form-control">
                {{ $errors->first('email') }}
            </div>
            <div class="form-group">
                <label for="message">Message:</label>
                <textarea name="message" id="" cols="30" rows="10" class="form-control">{{ old('message') }}</textarea>
                {{ $errors->first('message') }}
            </div>

            <button class="btn btn-primary">Send Email</button>
        </form>

    @endif

@endsection
